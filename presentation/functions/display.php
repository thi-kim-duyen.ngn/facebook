<?php
/******************************************************
Titre  : Apprendre à manipuler les médias dans une BDD
Auteur : NGN TKD
Date   : 27 Janvier 2020 - Version 1.0
Desc.  : Gestion d'images
*******************************************************/
$target_dir = "presentation/vue/tmp/";

// $files = glob($target_dir . "/*.*");

// var_dump($files);
$files = array();

if($dh = opendir($target_dir))
{
    while(false !== ($filename = readdir($dh))){
      if ($filename != "." && $filename != ".." ) {
        $files[] = $filename;
      }  
    }
    closedir($dh);
}

for ($i=0; $i < count($files); $i++)
{
    $image = $files[$i];
    $ext = strtolower(pathinfo($image, PATHINFO_EXTENSION));

    echo <<<_PRINT
    <div class="col-md-4">
    <div class="card mb-4 shadow-sm">
      <img class="bd-placeholder-img card-img-top" width="100%" height="350" src="presentation/vue/tmp/$image" alt="$image">
      <div class="card-body">
        <h3>Titre</h3>
        <p class="card-text">Description</p>
        <div class="d-flex justify-content-between align-items-center">
          <div class="btn-group">
            <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
          </div>
        <small class="text-muted">Jour/Heure</small>
        </div>
      </div>
    </div>
    </div>
    _PRINT;
}

?>