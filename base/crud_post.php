<?php

require_once("connection.php");

function AddPost($comment, $creationPost, $modificationPost){
    $sql = "INSERT INTO `Tbl_Post`(`Txt_Commentaire`, `Ts_CreationPost`, `Ts_ModificationPost`)
             VALUES(:comment, :creationPost, :modificationPost)";

    try{
        $db = connect();

        $id_Post = 0;  // Mettre à -1 lorsquon ajoutera les transactions
        $request = connect()->prepare($sql);
    
        $db->beginTransaction();
    
        $request->bindParam(":comment", $comment, PDO::PARAM_STR);
        $request->bindParam(":creationPost", $creationPost, PDO::PARAM_STR);
        $request->bindParam(":modificationPost", $modificationPost, PDO::PARAM_STR);
    
        $request->execute();
    
        $id_Post = connect()->lastInsertId(); 
    
        $result = $request->fetchAll(PDO::FETCH_ASSOC);
        
        $db->commit();

        return $id_Post; // !!!! Pas sûre qu'on devrait faire un return

    } catch(Exception $e){
        $db->rollBack();
        echo "Erreur : " . $e->getMessage();
    }
}


function ReadAllPost(){
    $sql = "SELECT Id_Post, Txt_Commentaire, Ts_CreationPost, Ts_ModificationPost
     FROM facebook.Tbl_Post";
    try {
        $db = connect();
        $request = connect()->prepare($sql);
        $db->beginTransaction();

        $request->execute();

        $result = $request->fetchAll(PDO::FETCH_ASSOC);
        $db->commit();

        return $result;
    } catch (\Exception $th) {
        $db->rollBack();
        echo "Erreur : " . $e->getMessage();
    }
}