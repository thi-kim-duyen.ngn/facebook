<?php
/******************************************************
Titre  : Apprendre à manipuler les médias dans une BDD
Auteur : NGN TKD
Date   : 27 Janvier 2020 - Version 1.0
Desc.  : Gestion des uploads
*******************************************************/
require_once('../../base/crud_post.php'); 
require_once('../../base/crud_media.php'); 

$comment = filter_input(INPUT_POST, "comment", FILTER_SANITIZE_STRING);

$creationPost = date('Y-m-d H:i:s');
$modificationPost = date('Y-m-d H:i:s');

$creationMedia = date('Y-m-d H:i:s');

$target_dir = "../vue/tmp/";
$countfiles = count(array_filter($_FILES['fileToUpload']['name']));

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {

    // Add a post
    $id_Post = AddPost($comment, $creationPost, $modificationPost);
    
    // Only upload if there's at least 1 file selected
    if ($countfiles != 0) {
        for($i = 0; $i < $countfiles; $i++){

            // Put time() so each imgs are unique
            $target_file = $target_dir . time() . '_' . basename($_FILES["fileToUpload"]["name"][$i]);
            
            $extension = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            $mediaName = pathinfo($target_file, PATHINFO_FILENAME);

            // Display a msg if it's a success or a fail
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $target_file)) {

                // Add a media
                AddMedia($extension, $mediaName, $creationMedia, $creationMedia, $id_Post);

                echo "The file ". basename( $_FILES["fileToUpload"]["name"][$i]) . " has been uploaded." . '<br>';

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    } else {
        header('Location: ../post.php');
        exit;
    }
}

?>