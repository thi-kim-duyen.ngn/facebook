<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Accueil</title>

    <link rel="stylesheet" href="./presentation/vue/css/style.css">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/album/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Custom styles for this template -->
    <!--<link href="./Album example · Bootstrap_files/album.css" rel="stylesheet">-->
  </head>
  <body>
    <header>
  <div class="collapse bg-dark" id="navbarHeader">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-md-7 py-4">
          <h4 class="text-white">Adresse</h4>
          <p class="text-muted"> CFPT - Secrétariat de l'Ecole d'Informatique </br>

            Bâtiment Rhône </br>
            10, chemin Gérard-de-Ternier </br>
            1213 Petit-Lancy </br>
            
            tél. +41 22 388 87 28 - fax. +41 22 388 87 65</p>
        </div>
        <div class="col-sm-4 offset-md-1 py-4">
          <h4 class="text-white">Informations</h4>
          <ul class="list-unstyled">
            <li><a href="https://edu.ge.ch/site/cfpt-informatique/" class="text-white">Site du CFPTi</a></li>
            <li><a href="https://edu.ge.ch/site/cfpt-informatique/lecole-dinformatique/contacter-lecole-dinformatique/" class="text-white">Nous contacter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar navbar-dark bg-dark shadow-sm">
    <div class="container" style="justify-content:left;">
      <a href="../../index.php">Home &nbsp; </a>
      <a href="#">Post</a>
      <div style="float: right; margin-left: auto;">
        <button style="float: right; " class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
          <span  style="float: right;" class="navbar-toggler-icon"></span>
        </button>
    </div>
    </div>
  </div>
</header>

<main role="main">

<section class="jumbotron text-center">
    <div class="container">
      <h1>Share a thought</h1>
      <form class="form center-block" action="../functions/upload.php" method="post" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="form-group">
                <textarea class="form-control input-lg" autofocus="" placeholder="What do you want to share?" name="comment" style="resize: none; maxlength: 250; height: 100px;"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <ul class="pull-left list-inline"><input type="file" name="fileToUpload[]" accept="image/gif, image/jpeg, image/png, image/jpg, video/*, audio/*" multiple></li></ul>
            <input type="submit" name="submit" value="Post" class="btn btn-primary btn-sm">
        </div>
      </form>
    </div>
</section>

</main>

<footer class="text-muted">
  <div class="container">
    <p> &copy; Nguyen Thi-kim</p>
  </div>
</footer>
<!-- <script src="./Album example · Bootstrap_files/jquery-3.4.1.slim.min.js.téléchargement" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
<script src="./Album example · Bootstrap_files/bootstrap.bundle.min.js.téléchargement" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script> -->
</body>
</html>
