<?php

require_once("connection.php");

// Add a media
function AddMedia($extension, $mediaName, $creationMedia, $modificationMedia, $fkpost){
    $sql = "INSERT INTO `Tbl_Media`(`Txt_TypeMedia`, `Txt_NomMedia`, `Ts_CreationMedia`, `Ts_ModificationMedia`, `Fk_Post`)
            VALUES(:extension, :mediaName, :creationMedia, :modificationMedia, :fkpost)";
    
    try{ 
        $db = connect();

        $request = connect()->prepare($sql);

        $db->beginTransaction();

        $request->bindParam(":extension", $extension, PDO::PARAM_STR);
        $request->bindParam(":mediaName", $mediaName, PDO::PARAM_STR);
        $request->bindParam(":creationMedia", $creationMedia, PDO::PARAM_STR);
        $request->bindParam(":modificationMedia", $modificationMedia, PDO::PARAM_STR);
        $request->bindParam(":fkpost", $fkpost, PDO::PARAM_STR);
    
        $request->execute(); 
    
        $result = $request->fetchAll(PDO::FETCH_ASSOC);
    
        $db->commit();

        return $result; // !!!! Pas sûre qu'on devrait faire un return

    } catch(Exception $e){
        $db->rollBack();
        echo "Erreur : " . $e->getMessage();
    }

}

function ReadAllMedia(){
    $sql = "SELECT Id_Media, Txt_TypeMedia, Txt_NomMedia, Ts_CreationMedia, Ts_ModificationMedia, Fk_Post 
    FROM facebook.Tbl_Media";
    try {
        $db = connect();
        $request = connect()->prepare($sql);
        $db->beginTransaction();

        $request->execute();

        $result = $request->fetchAll(PDO::FETCH_ASSOC);
        $db->commit();

        return $result;
    } catch (\Exception $th) {
        $db->rollBack();
        echo "Erreur : " . $e->getMessage();
    }
}