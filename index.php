<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Accueil</title>

    <link rel="stylesheet" href="./presentation/vue/css/style.css">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/album/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Custom styles for this template -->
    <!--<link href="./Album example · Bootstrap_files/album.css" rel="stylesheet">-->
  </head>
  <body>
    <header>
  <div class="collapse bg-dark" id="navbarHeader">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-md-7 py-4">
          <h4 class="text-white">Adresse</h4>
          <p class="text-muted"> CFPT - Secrétariat de l'Ecole d'Informatique </br>

            Bâtiment Rhône </br>
            10, chemin Gérard-de-Ternier </br>
            1213 Petit-Lancy </br>
            
            tél. +41 22 388 87 28 - fax. +41 22 388 87 65</p>
        </div>
        <div class="col-sm-4 offset-md-1 py-4">
          <h4 class="text-white">Informations</h4>
          <ul class="list-unstyled">
            <li><a href="https://edu.ge.ch/site/cfpt-informatique/" class="text-white">Site du CFPTi</a></li>
            <li><a href="https://edu.ge.ch/site/cfpt-informatique/lecole-dinformatique/contacter-lecole-dinformatique/" class="text-white">Nous contacter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar navbar-dark bg-dark shadow-sm">
    <div class="container" style="justify-content:left;">
      <a href="#">Home &nbsp; </a>
      <a href="./presentation/vue/post.php">Post</a>
      <div style="float: right; margin-left: auto;">
        <button style="float: right; " class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
          <span  style="float: right;" class="navbar-toggler-icon"></span>
        </button>
    </div>
    </div>
  </div>
</header>

<main role="main">

  <section class="jumbotron text-center">
      <h1>This is your wall</h1>
  </section>

  <div class="album py-5 bg-light">
  <div class="container">

    <div class="row">

        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <img class="bd-placeholder-img card-img-top" width="100%" height="350" src="./presentation/vue/img/cherrykirby.png" alt="cherrykirby">
            <div class="card-body">
              <h3>Sunset</h3>
              <p class="card-text">Description</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                  <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                </div>
                <small class="text-muted">Jour/Heure</small>
              </div>
            </div>
          </div>
        </div>

        <?php require_once("./presentation/functions/display.php") ?>

    </div>

 </div>

</main>

<footer class="text-muted">
  <div class="container">
    <p class="float-right">
      <a href="#">Back to top</a>
    </p>
    <p> &copy; Nguyen Thi-kim</p>
  </div>
</footer>
<!-- <script src="./Album example · Bootstrap_files/jquery-3.4.1.slim.min.js.téléchargement" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
<script src="./Album example · Bootstrap_files/bootstrap.bundle.min.js.téléchargement" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script> -->
</body>
</html>